#include <epoxy/gl.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

void APIENTRY gl_error_callback(
	GLenum source,
	GLenum type,
	GLuint id,
	GLenum severity,
	GLsizei length,
	const GLchar* message,
	const void* userParam
)
{
	std::cerr << "OpenGL Error occured:" << std::endl;

	std::cerr << "  Source: ";
	switch (source)
	{
		case GL_DEBUG_SOURCE_API:
			std::cerr << "SOURCE_API";
			break;
		case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
			std::cerr << "WINDOW_SYSTEM";
			break;
		case GL_DEBUG_SOURCE_SHADER_COMPILER:
			std::cerr << "SHADER_COMPILER";
			break;
		case GL_DEBUG_SOURCE_THIRD_PARTY:
			std::cerr << "SOURCE_THIRD_PARTY";
			break;
		case GL_DEBUG_SOURCE_APPLICATION:
			std::cerr << "APPLICATION";
			break;
		case GL_DEBUG_SOURCE_OTHER:
			std::cerr << "OTHER";
			break;
	}
	std::cerr << std::endl;

	std::cerr << "  Type: ";
	switch (type)
	{
		case GL_DEBUG_TYPE_ERROR:
			std::cerr << "ERROR";
			break;
		case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
			std::cerr << "DEPRECATED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
			std::cerr << "UNDEFINED_BEHAVIOR";
			break;
		case GL_DEBUG_TYPE_PORTABILITY:
			std::cerr << "PORTABILITY";
			break;
		case GL_DEBUG_TYPE_PERFORMANCE:
			std::cerr << "PERFORMANCE";
			break;
		case GL_DEBUG_TYPE_OTHER:
			std::cerr << "OTHER";
			break;
	}
	std::cerr << std::endl;

	std::cerr << "  ID: " << id << std::endl;

	std::cerr << "  Severity:  ";
	switch (severity)
	{
		case GL_DEBUG_SEVERITY_HIGH:
			std::cerr << "High";
			break;
		case GL_DEBUG_SEVERITY_MEDIUM:
			std::cerr << "Medium";
			break;
		case GL_DEBUG_SEVERITY_LOW:
			std::cerr << "Low";
			break;
		default:
			std::cerr << severity;
			break;
	}
	std::cerr << std::endl;

	std::cerr << "  Message: " << message << std::endl;
}

GLuint make_shader(GLenum shader_type, const std::string &source_path)
{
	std::cout << "Make shader " << source_path << std::endl;

	std::ifstream file(source_path);
	std::stringstream buffer;
	buffer << file.rdbuf();
	buffer << '\0';

	std::string source = buffer.str();
	const char *c_str = source.c_str();

	GLuint shader = glCreateShader(shader_type);

	glShaderSource(shader, 1, &c_str, nullptr);
	glCompileShader(shader);

	GLint success = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE)
	{
		GLint log_size = 0;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &log_size);

		std::vector<GLchar> log(log_size);

		glGetShaderInfoLog(shader, log_size, nullptr, &log[0]);

		std::cerr << &log[0] << std::endl;

		glDeleteShader(shader);
		shader = 0;
	}

	return shader;
}

GLuint make_program(const std::vector<GLuint> &shaders)
{
	GLuint program = glCreateProgram();
	
	for (size_t i = 0; i < shaders.size(); ++i)
	{
		glAttachShader(program, shaders[i]);
	}

	glLinkProgram(program);

	GLint success = 0;
	glGetProgramiv(program, GL_LINK_STATUS, (int *)&success);
	if (success == GL_FALSE)
	{
		GLint log_size = 0;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &log_size);

		std::vector<GLchar> log(log_size);
		glGetProgramInfoLog(program, log_size, &log_size, &log[0]);

		glDeleteProgram(program);
		program = 0;
	}

	return program;
}

int main(int argc, char** args)
{
	if (!glfwInit())
	{	
		std::cerr << "GLFW init failed" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	glfwWindowHint(GLFW_RESIZABLE, true);

	GLFWwindow *window = nullptr;
	window = glfwCreateWindow(1280, 720, "Iloinen ananas", NULL, NULL);
	if (!window)
	{
		std::cerr << "Window creation failed" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	glfwMakeContextCurrent(window);

	glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
	glDebugMessageCallback(&gl_error_callback, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);

	std::cout << "Make shaders..." << std::endl;	
	GLuint shader_vertex_empty = make_shader(GL_VERTEX_SHADER, "empty.vert.glsl");
	GLuint shader_geometry_fullscreenquad = make_shader(GL_GEOMETRY_SHADER, "fullscreenquad.geom.glsl");
	GLuint shader_fragment_effu1 = make_shader(GL_FRAGMENT_SHADER, "effu1.frag.glsl");

	std::cout << "Make programs..." << std::endl;
	GLuint shaderProg_effu1 = make_program({ shader_vertex_empty, /* shader_geometry_fullscreenquad, */ shader_fragment_effu1});
	GLuint uniform_aspect = glGetUniformLocation(shaderProg_effu1, "aspect");
	GLuint uniform_time = glGetUniformLocation(shaderProg_effu1, "time");
	GLuint uniform_time_max = glGetUniformLocation(shaderProg_effu1, "time_max");

	std::cout << "Make meshes..." << std::endl;

	glm::vec3 quad_verts[] =
	{
		glm::vec3(-1.0f,  1.0f, 0.0f), // Top left
		glm::vec3( 1.0f,  1.0f, 0.0f), // Top right
		glm::vec3( 1.0f, -1.0f, 0.0f), // Bottom right
		glm::vec3(-1.0f, -1.0f, 0.0f)  // Bottom left
	};
	
	// Unused
	glm::uvec3 quad_tris[] =
	{
		glm::uvec3(0, 1, 2),
		glm::uvec3(2, 3, 0)
	};

	GLuint vao_quad = 0;
	glGenVertexArrays(1, &vao_quad);
	glBindVertexArray(vao_quad);

	GLuint vbo_quad = 0;
	glGenBuffers(1, &vbo_quad);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_quad);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec3), quad_verts, GL_STATIC_DRAW);
	GLint attribLoc_pos = glGetAttribLocation(shaderProg_effu1, "pos");
	glVertexAttribPointer(attribLoc_pos, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(attribLoc_pos);
	
	size_t frame_index = 0;
	
	std::cout << "Start rendering" << std::endl;
	while (!glfwWindowShouldClose(window))
	{
		glm::ivec2 window_size;
		glfwGetWindowSize(window, &window_size.x, &window_size.y);
		float window_aspect = float(window_size.x) / float(window_size.y);
		float time = (float)glfwGetTime();
		float time_max = 60.f;

		glViewport(0, 0, window_size.x, window_size.y);

		glClearColor(0, 0, 0, 1);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(shaderProg_effu1);
		glUniform1f(uniform_aspect, window_aspect);
		glUniform1f(uniform_time, time);
		glUniform1f(uniform_time_max, time_max);
		glBindVertexArray(vao_quad);
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		glfwSwapBuffers(window);
		glfwPollEvents();

		++frame_index;

		if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) // || time >= 20.f)
			break;
	}

	glfwTerminate();
	return 0;
}

