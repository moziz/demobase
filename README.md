# Demobase #

Minimal OpenGL demo base project. Renders a fullscreen fragment shader effect.

Made at Assembly Summer 2019 for use in the One Scene Compo. Cleaned up and improved afterwards.

# External dependencies #
libepoxy, glfw, glm

