#version 430 core

uniform float aspect;

in vec3 pos;
out vec2 uv;

void main() 
{
/*
    float x = float(((uint(gl_VertexID) + 2u) / 3u)%2u); 
    float y = float(((uint(gl_VertexID) + 1u) / 3u)%2u); 

    gl_Position = vec4(-1.0f + x * 2.0f, -1.0f + y * 2.0f, 0.0f, 1.0f);
    uv = vec2(x, y);
*/ 
	gl_Position = vec4(pos, 1.0);
	uv = vec2(pos.x, pos.y);
}

