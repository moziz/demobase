#version 430 core

const float pi = 3.141;

uniform float aspect;
uniform float time;
uniform float time_max;

in vec2 uv;
out vec4 out_color;

void main()
{
	float density = 0;
	float dist_min = 1.0 / 0.0;
	int circle_count = 128;

	for (int i = 0; i < circle_count; ++i)
	{
		float it = float(i) / circle_count;
		float r = 0.25 + sin(it * (2 + 1.4 * cos(time * 2) ) * pi + time * 6) * 0.125;

		vec2 p = vec2(uv.x * aspect, uv.y);
		p.x = p.x + sin(time * 2.0 + it * 2 * pi) * 1.5 * r + cos(time * pi + sin(time * 0.5)) * 0.35;
		p.y = p.y + cos(time * 2.3 + it * 2 * pi) * 1.5 * r + sin(time * pi + sin(time * 0.4)) * 0.25;
	
		float distance = length(p) - r;
		if (distance < 0)
		{
			density += 1.0 / float(circle_count / 2);
		}

		dist_min = min(dist_min, distance);
	}

	if (dist_min > 0)
	{
		float a = dist_min / length(vec2(2,2));
		density += min(a, 1.0) * sin(dist_min * 16 * pi * (1 - dist_min / 4) - time * 32);
	}

	out_color = vec4(density, density, density, 1);
}

